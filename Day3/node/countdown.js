const countdown = (angka) => {
  let a = setInterval(() => {
    let hours = Math.floor(angka / 3600);
    let minutes = Math.floor((angka % 3600) / 60);
    let seconds = Math.floor((angka % 3600) % 60);

    console.log(`${hours}:${minutes}:${seconds}`);
    angka--;
    if (angka < 0) {
      clearInterval(a);
    }
  }, 1000);
};

countdown(50000);
