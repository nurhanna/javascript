import fetch from "node-fetch";

// 1. Show all list title
async function getListTitle() {
  const response = await fetch(
    "https://api.jikan.moe/v4/recommendations/anime",
    { method: "GET" }
  );
  const animes = await response.json();
  const anime = animes.data.slice(0, 10);

  const entry = anime.map((data) => data.entry).flat();
  // console.log(entry);

  const title = entry.map((data) => data.title);
  console.log("==list title");
  console.log(title);
}

// getListTitle();

// ========================================================================================================================================

// 2. Sort title by date release
async function sortTitle() {
  const response = await fetch(
    "https://api.jikan.moe/v4/recommendations/anime",
    { method: "GET" }
  );
  const animes = await response.json();
  const anime = animes.data.slice(0, 3);

  const entry = anime.map((data) => data.entry).flat();
  const mal_id = entry.map((a) => a.mal_id);

  let a = [];
  for (let i = 0; i < mal_id.length; i++) {
    const detail = await fetch(`https://api.jikan.moe/v4/anime/${mal_id[i]}`);
    let hasil = await detail.json();
    a.push(hasil);
  }
  const title = a.map((a) => a.data.title);
  const aired = a.map((data) => data.data.aired).flat();
  const date = aired.map((a) => a.from);

  console.log(title);
  console.log(date);
  let final = [];

  for (let j = 0; j < title.length; j++) {
    let obj = {};
    obj["title"] = title[j];
    obj["date"] = date[j];
    final.push(obj);
  }

  console.log(final);

  const final_sort = final.sort(
    (a, b) => Date.parse(b.date) - Date.parse(a.date)
  );
  const title_saja = final_sort.map((a) => a.title);
  console.log("================================================");
  console.log("Hasil sort anime with date");
  console.log(final_sort);
  console.log("sort title");
  console.log(title_saja);
}
// sortTitle();

// =======================================================================================================================================

// 3. Show 5 most popular anime from recommendation
async function popularAnime() {
  const response = await fetch(
    "https://api.jikan.moe/v4/recommendations/anime",
    { method: "GET" }
  );
  const animes = await response.json();
  const anime = animes.data.slice(0, 3);

  const entry = anime.map((data) => data.entry).flat();
  const mal_id = entry.map((a) => a.mal_id);

  let a = [];
  for (let i = 0; i < mal_id.length; i++) {
    const detail = await fetch(`https://api.jikan.moe/v4/anime/${mal_id[i]}`);
    let hasil = await detail.json();
    a.push(hasil);
  }
  const title = a.map((a) => a.data.title);
  const popularity = a.map((a) => a.data.popularity);

  // console.log(title);
  // console.log(popularity);

  let final = [];

  for (let j = 0; j < title.length; j++) {
    let obj = {};
    obj["title"] = title[j];
    obj["popularity"] = popularity[j];
    final.push(obj);
  }
  console.log(final);
  const top5 = final.sort((a, b) => b.popularity - a.popularity).slice(0, 5);
  const title_populer = top5.map((a) => a.title);
  console.log("==========================================================");
  console.log("Top 5 Populer Anime");
  // console.log(top5);
  console.log(title_populer);
}

// popularAnime();

// ========================================================================================================================================

// 4. Show 5 high rank anime from recommendation
async function highRankAnime() {
  const response = await fetch(
    "https://api.jikan.moe/v4/recommendations/anime",
    { method: "GET" }
  );
  const animes = await response.json();
  const anime = animes.data.slice(0, 3);

  const entry = anime.map((data) => data.entry).flat();
  const mal_id = entry.map((a) => a.mal_id);

  let a = [];
  for (let i = 0; i < mal_id.length; i++) {
    const detail = await fetch(`https://api.jikan.moe/v4/anime/${mal_id[i]}`);
    let hasil = await detail.json();
    a.push(hasil);
  }
  const title = a.map((a) => a.data.title);
  const rank = a.map((a) => a.data.rank);

  // console.log(title);
  // console.log(rank)

  let final = [];

  for (let j = 0; j < title.length; j++) {
    let obj = {};
    obj["title"] = title[j];
    obj["rank"] = rank[j];
    final.push(obj);
  }
  // console.log(final);

  const top5 = final.sort((a, b) => b.rank - a.rank).slice(0, 5);
  const title_high_rank = top5.map((a) => a.title);
  console.log("==========================================================");
  console.log("Top 5 high rank Anime");
  // console.log(top5);
  console.log(title_high_rank);
}

// highRankAnime();

// ========================================================================================================================================
// 5. Show most episodes anime from recommendation
async function mostEpisode() {
  const response = await fetch(
    "https://api.jikan.moe/v4/recommendations/anime",
    { method: "GET" }
  );
  const animes = await response.json();
  const anime = animes.data.slice(0, 3);
  // console.log(anime);

  const entry = anime.map((data) => data.entry).flat();
  const mal_id = entry.map((a) => a.mal_id);

  let a = [];
  for (let i = 0; i < mal_id.length; i++) {
    const detail = await fetch(`https://api.jikan.moe/v4/anime/${mal_id[i]}`);
    let hasil = await detail.json();
    a.push(hasil);
  }
  const title = a.map((a) => a.data.title);
  const episodes = a.map((a) => a.data.episodes);

  // console.log(title);
  // console.log(episodes);

  let final = [];

  for (let j = 0; j < title.length; j++) {
    let obj = {};
    obj["title"] = title[j];
    obj["episodes"] = episodes[j];
    final.push(obj);
  }

  const top5 = final.sort((a, b) => b.episodes - a.episodes);
  console.log(top5);
  console.log("==========================================================");
  console.log("most episodes anime");
  console.log(top5.slice(0, 1));
}

mostEpisode();
