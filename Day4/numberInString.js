const numInStr = (arr) => {
  let newArr = [];
  for (let i = 0; i < arr.length; i++) {
    for (let j = 0; j < arr[i].length; j++) {
      if (arr[i][j] == " ") {
        break;
      }
      if (!isNaN(arr[i][j])) {
        newArr.push(arr[i]);
        break;
      }
    }
  }
  console.log(newArr);
};

numInStr(["1a", "a", "2b", "b"]);
numInStr(["abc", "abc10"]);
numInStr(["abc", "ab10c", "a10bc", "bcd"]);
numInStr(["this is a test", "test1"]);
numInStr(["abc", "ab10c", "a10bc", "bcd"]);
numInStr(["1", "a", " ", "b"]);
numInStr(["rct", "ABC", "Test", "xYz"]);
numInStr(["this IS", "10xYZ", "xy2K77", "Z1K2W0", "xYz"]);
numInStr(["-/>", "10bc", "abc "]);
