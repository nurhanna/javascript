const balanced = (word) => {
  let word_length = word.length;
  let awal = 0;
  let akhir = 0;
  var alphabet = [
    "a",
    "b",
    "c",
    "d",
    "e",
    "f",
    "g",
    "h",
    "i",
    "j",
    "k",
    "l",
    "m",
    "n",
    "o",
    "p",
    "q",
    "r",
    "s",
    "t",
    "u",
    "v",
    "w",
    "x",
    "y",
    "z",
  ];

  if (word_length % 2 == 0) {
    for (let i = 0; i < word_length / 2; i++) {
      awal += alphabet.indexOf(word[i]) + 1;
    }

    for (let i = word_length / 2; i < word_length; i++) {
      akhir += alphabet.indexOf(word[i]) + 1;
    }
  } else if (word_length % 2 != 0) {
    for (let i = 0; i < (word_length - 1) / 2; i++) {
      awal += alphabet.indexOf(word[i]) + 1;
    }

    for (let i = (word_length + 1) / 2; i < word_length; i++) {
      akhir += alphabet.indexOf(word[i]) + 1;
    }
  }

  if (awal == akhir) {
    console.log(true);
  } else {
    console.log(false);
  }
};

balanced("zips");
balanced("brake");
balanced("at");
balanced("forgetful");
balanced("vegetation");
balanced("disillusioned");
balanced("abstract");
balanced("clever");
balanced("conditionalities");
balanced("seasoning");
balanced("uptight");
balanced("ticket");
balanced("calculate");
balanced("measure");
balanced("join");
balanced("anesthesiologies");
balanced("command");
balanced("graphite");
balanced("quadratically");
balanced("right");
balanced("fossil");
balanced("sparkling");
balanced("dolphin");
balanced("baseball");
balanced("immense");
balanced("pattern");
balanced("hand");
balanced("radar");
balanced("oven");
balanced("immutability");
balanced("kayak");
balanced("bartender");
balanced("weight");
balanced("lightbulbs");
balanced("tail");
