const operation = (a, b, value) => {
  value = value.toLowerCase();
  a = Number(a);
  b = Number(b);
  switch (value) {
    case "add":
      console.log(a + b);
      break;
    case "subtract":
      console.log(a - b);
      break;
    case "divide":
      c = a / b;
      if (c == Infinity) {
        c = "undefined";
      }
      console.log(c);
      break;
    case "multiply":
      console.log(a * b);
      break;
    default:
      console.log("inputan tidak sesuai");
  }
};

operation("1", "2", "add");
operation("4", "5", "subtract");
operation("6", "3", "divide");
operation("2", "7", "multiply");
operation("6", "0", "divide");
