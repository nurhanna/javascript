const leftSlide = (array) => {
  arr = array.filter((angka) => angka !== 0);
  let out = [];

  for (let i = 0; i < arr.length; i++) {
    let currNum = arr[i];
    if (i + 1 < arr.length && currNum === arr[i + 1]) {
      let merNum = currNum * 2;
      out.push(merNum);
      i += 1;
    } else {
      out.push(currNum);
    }
  }

  while (out.length < array.length) {
    out.push(0);
  }

  console.log(out);
};

leftSlide([2, 2, 2, 0]);
leftSlide([2, 2, 4, 4, 8, 8]);
leftSlide([0, 2, 0, 2, 4]);
leftSlide([0, 2, 2, 8, 8, 8]);
leftSlide([2, 2, 2, 0]);
leftSlide([2, 2, 4, 4, 8, 8]);
leftSlide([0, 2, 0, 2, 4]);
leftSlide([0, 2, 2, 8, 8, 8]);
leftSlide([0, 0, 0, 0]);
leftSlide([0, 0, 0, 2]);
leftSlide([2, 0, 0, 0]);
leftSlide([8, 2, 2, 4]);
leftSlide([1024, 1024, 1024, 512, 512, 256, 256, 128, 128, 64, 32, 32]);
