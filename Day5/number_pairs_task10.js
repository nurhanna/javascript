// 10. Create a function that determines how many number pairs are embedded in a space-separated string. The first numeric value in the space-separated string represents the count of the numbers, thus, excluded in the pairings.
// Notes
// Always take into consideration the first number in the string is not part of the pairing, thus, the count. It may not seem so useful as most people see it, but it's mathematically significant if you deal with set operations.

const number_pairs = (input) => {
  let n = 0;
  //   ubah ke array
  arr = input.split(" ");
  //   delete element pertama
  arr.shift();
  arr = arr.sort();
  for (let i = 0; i < arr.length - 1; i++) {
    if (arr[i] == arr[i + 1]) {
      n += 1;
      i += 1;
    } else {
      i = i;
    }
  }
  console.log(n);
};

// sukses semua test case

number_pairs("7 1 2 1 2 1 3 2"); //➞ 2 // (1, 1) ➞ (2, 2)
number_pairs("9 10 20 20 10 10 30 50 10 20"); //➞ 3 // (10, 10) ➞ (20, 20) ➞ (10, 10)
number_pairs("4 2 3 4 1"); //➞ 0 // Although two 4's are present, the first one is discounted.
