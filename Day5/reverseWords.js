// 7. Given an input string, reverse the string word by word, the first word will be the last, and so on.
// Notes
// A word is defined as a sequence of non-space characters.
// The input string may contain leading or trailing spaces. However, your reversed string should not contain leading or trailing spaces.
// You need to reduce multiple spaces between two words to a single space in the reversed string.
// Try to solve this in linear time.

const reverseWords = (arr) => {
  arr = arr.split(" ");
  let newArr = [];
  for (i = arr.length - 1; i >= 0; i--) {
    if (arr[i] != "") {
      newArr.push(arr[i]);
    }
  }
  console.log(newArr.join(" "));
};

reverseWords(" the sky is blue"); //➞ "blue is sky the"
reverseWords("hello   world!  "); //➞ "world! hello"
reverseWords("a good example"); //➞ "example good a"
reverseWords("hello world!"); //➞ "world! hello")
reverseWords("blue is sky the"); //➞ "the sky is blue")
reverseWords("a good example"); //➞ "example good a")
reverseWords("fraud! of example another is this"); //➞ "this is another example of fraud!")
reverseWords("man! the are You"); //➞ "You are the man!")
