// A countdown sequence is a descending sequence of k integers from k down to 1 (e.g. 5, 4, 3, 2, 1). Write a function that returns an array of the form[x, y] where x is the number of countdown sequences in the given array and y is the array of those sequences in the order in which they appear in the array.
// Notes
// A disjoint 1 is a valid countdown sequence. See examples #3 & #4.
// All numbers in the array will be greater than 0.

const finalCountdown = (arr) => {
  let n = 0;
  let newArr = [];
  arr = arr.reverse();

  for (let i = 0; i < arr.length; i++) {
    let temp_arr = [];

    if (arr[i] == 1) {
      n += 1;
      temp_arr.push(arr[i]);

      for (let j = i; j < arr.length; j++) {
        if (arr[j + 1] - arr[j] == 1) {
          temp_arr.push(arr[j + 1]);
        } else {
          j = arr.length;
        }
      }

      if (temp_arr.length != 0) {
        newArr.push(temp_arr.reverse());
      }
    }
  }

  let newArr2 = [];
  newArr2 = newArr.reverse(newArr);

  let output_arr = [];
  output_arr.push(n);
  output_arr.push(newArr2);
  console.log(output_arr);
};

// all test case success

finalCountdown([4, 8, 3, 2, 1, 2]);
// ➞ [1, [[3, 2, 1]]]
finalCountdown([4, 4, 5, 4, 3, 2, 1, 8, 3, 2, 1]);
// ➞ [2, [[5, 4, 3, 2, 1], [3, 2, 1]]]
finalCountdown([4, 3, 2, 1, 3, 2, 1, 1]);
// ➞ [3, [[4, 3, 2, 1], [3, 2, 1], [1]]]
finalCountdown([1, 1, 2, 1]);
//  ➞ [3, [[1], [1], [2, 1]]]
finalCountdown([]);
// ➞  [0, []]
finalCountdown([5, 4, 3, 2, 1]);
// ➞ [1, [[5, 4, 3, 2, 1]]]
finalCountdown([2, 5, 4, 3, 2, 1, 2]);
//  ➞ [1, [[5, 4, 3, 2, 1]]]
finalCountdown([2, 3, 2, 1, 4, 3, 2, 1]);
// ➞ [2, [[3, 2, 1], [4, 3, 2, 1]]]
finalCountdown([4, 3, 2, 5, 4, 3]);
// ➞ [0, []]
finalCountdown([4, 3, 2, 5, 4, 3, 1]);
// ➞ [1, [[1]]]
finalCountdown([3, 2, 1, 5, 5, 3, 2, 1, 5, 5]);
//  ➞ [2, [[3, 2, 1], [3, 2, 1]]]
finalCountdown([4, 8, 3, 2, 1, 2]);
//  ➞ [1, [[3, 2, 1]]]
finalCountdown([4, 4, 5, 4, 3, 2, 1, 8, 3, 2, 1]);
// ➞ [2, [[5, 4, 3, 2, 1], [3, 2, 1]]]
finalCountdown([4, 3, 2, 1, 3, 2, 1, 1]);
//  ➞ [3, [[4, 3, 2, 1], [3, 2, 1], [1]]]
finalCountdown([1, 2, 1, 1]);
// ➞ [3, [[1], [2, 1], [1]]]
finalCountdown([1, 2, 3, 4, 3, 2, 1]);
//  ➞ [2, [[1], [4, 3, 2, 1]]]
finalCountdown([]);
// ➞ [0, []]
finalCountdown([2, 1, 2, 1]);
//  ➞ [2, [[2, 1], [2, 1]]]

// // PENJELASAN :

// const finalCountdown = (arr) => {
//     let n = 0;
//     let newArr = [];
//     arr = arr.reverse(); //reverse biar gampang

//     for (let i = 0; i < arr.length; i++) {
//       //   tempat tampung sementara yang berurutan ex. 1 2 3 4
//       let temp_arr = [];

//       //ketemu angka 1
//       if (arr[i] == 1) {
//         n += 1; //maka tambahkan n
//         temp_arr.push(arr[i]); //push dulu 1 nya

//         //masuk ke looping baru dengan looping mulai arr[i] atau setelah nemu angka 1
//         for (let j = i; j < arr.length; j++) {
//           //cek apakah arr setelahnya - arr sekarang(a[j]) selisih satu
//           if (arr[j + 1] - arr[j] == 1) {
//             temp_arr.push(arr[j + 1]); //jika ya push arr setelahnya
//           } else {
//             // jika sudah tidak selisih 1 lagi hentikan loop dengan ngasih j sampai batasan akhir
//             j = arr.length;
//           }
//         }

//         //   jika temp ada isi push ke new arr, reverse ex. [1,2,3] jadi [3,2,1]
//         if (temp_arr.length != 0) {
//           newArr.push(temp_arr.reverse()); //direverse balik ke awal
//         }
//       }
//     }

//     //   new arr 2 untuk reverse arraynya kembali ex. [  [ 3, 2, 1 ] , [ 5, 4, 3, 2, 1 ]] ] jadi  [ [ 5, 4, 3, 2, 1 ], [ 3, 2, 1 ] ] ]
//     let newArr2 = [];
//     newArr2 = newArr.reverse(newArr);

//     //   output_arr untuk gabungakna dengan n
//     let output_arr = [];
//     output_arr.push(n);
//     output_arr.push(newArr2);
//     console.log(output_arr);
//   };
