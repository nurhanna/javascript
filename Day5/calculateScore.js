// Abigail and Benson are playing Rock, Paper, Scissors.
// Each game is represented by an array of length 2, where the first element represents what Abigail played and the second element represents what Benson played.

// Given a sequence of games, determine who wins the most number of matches. If they tie, output "Tie".

// R stands for Rock
// P stands for Paper
// S stands for Scissors

const calculateScore = (arr) => {
  let Abigail = 0;
  let Benson = 0;
  for (let i = 0; i < arr.length; i++) {
    if (arr[i][0] == arr[i][1]) {
      Abigail += 0;
      Benson += 0;
    } else if (arr[i][0] == "R" && arr[i][1] == "P") {
      Benson += 1;
    } else if (arr[i][0] == "R" && arr[i][1] == "S") {
      Abigail += 1;
    } else if (arr[i][0] == "P" && arr[i][1] == "R") {
      Abigail += 1;
    } else if (arr[i][0] == "P" && arr[i][1] == "S") {
      Benson += 1;
    } else if (arr[i][0] == "S" && arr[i][1] == "R") {
      Benson += 1;
    } else if (arr[i][0] == "S" && arr[i][1] == "P") {
      Abigail += 1;
    }
  }
  if (Abigail == Benson) {
    console.log("Tie");
  } else if (Abigail > Benson) {
    console.log("Abigail");
  } else if (Abigail < Benson) {
    console.log("Benson");
  }
};

calculateScore([
  ["R", "R"],
  ["S", "S"],
]);
calculateScore([
  ["S", "R"],
  ["R", "S"],
  ["R", "R"],
]);
calculateScore([
  ["R", "P"],
  ["R", "S"],
  ["S", "P"],
]);
calculateScore([
  ["R", "R"],
  ["S", "S"],
]);
calculateScore([
  ["S", "R"],
  ["R", "S"],
  ["R", "R"],
]);
calculateScore([
  ["S", "R"],
  ["P", "R"],
]);
calculateScore([
  ["S", "S"],
  ["S", "P"],
  ["R", "S"],
  ["S", "R"],
  ["R", "R"],
]);
calculateScore([
  ["S", "R"],
  ["S", "R"],
  ["S", "R"],
  ["R", "S"],
  ["R", "S"],
]);
