// Create a function that changes specific words into emoticons. Given a sentence as a string, replace the words smile, grin, sad and mad with their corresponding emoticons

// word	emoticon
// smile	:D
// grin	:)
// sad	:(
// mad	:P

// The sentence always starts with "Make me".
// Try to solve this without using conditional statements like if/else or switch.

const emotify = (stc) => {
  arr = stc.split(" ");
  switch (arr[2]) {
    case "smile":
      console.log("Make me :D");
      break;
    case "grin":
      console.log("Make me :)");
      break;
    case "sad":
      console.log("Make me :(");
      break;
    case "mad":
      console.log("Make me :P");
      break;
    default:
      console.log("wrong sentence");
  }
};
emotify("Make me smile");
emotify("Make me grin");
emotify("Make me sad");
emotify("Make me smile");
emotify("Make me grin");
emotify("Make me sad");
emotify("Make me mad");
