// ===================================================================================================================
// Create a function that takes an array of numbers between 1 and 10 (excluding one number) and returns the missing number.
// Notes
// The array of numbers will be unsorted (not in order).
// Only one number will be missing.

const missingNum = (arr) => {
  let miss = 0;
  for (let i = 1; i <= 10; i++) {
    if (!arr.some((a) => a == i)) {
      miss = i;
      i = 10;
      break;
    }
  }
  console.log(miss);
};

missingNum([1, 2, 3, 4, 6, 7, 8, 9, 10]);
missingNum([7, 2, 3, 6, 5, 9, 1, 4, 8]);
missingNum([10, 5, 1, 2, 4, 6, 8, 3, 9]);
missingNum([1, 2, 3, 4, 6, 7, 8, 9, 10]);
missingNum([7, 2, 3, 6, 5, 9, 1, 4, 8]);
missingNum([7, 2, 3, 9, 4, 5, 6, 8, 10]);
missingNum([10, 5, 1, 2, 4, 6, 8, 3, 9]);
missingNum([1, 7, 2, 4, 8, 10, 5, 6, 9]);
