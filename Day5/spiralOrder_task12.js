// Given a matrix of m * n elements (m rows, n columns) ➞ return all elements of the matrix in spiral order.

const spiralOrder = (arr) => {
  let newArr = [];

  newArr.push(arr[0]);
  newArr.push(arr[1][arr[1].length - 1]);
  for (let i = arr[2].length - 1; i >= 0; i--) {
    newArr.push(arr[2][i]);
  }
  for (let i = 0; i < arr[1].length - 1; i++) {
    newArr.push(arr[1][i]);
  }
  newArr = newArr.flat();
  console.log(newArr);
};

spiralOrder([
  [1, 2, 3],
  [4, 5, 6],
  [7, 8, 9],
]);

spiralOrder([
  [1, 2, 3, 4],
  [5, 6, 7, 8],
  [9, 10, 11, 12],
]);

spiralOrder([
  [1, 2, 3, 4],
  [5, 6, 7, 8],
  [9, 10, 11, 12],
]);

spiralOrder([
  [1, 2, 3, 4, 5],
  [6, 7, 8, 9, 10],
  [11, 12, 13, 14, 15],
]);
// ➞ [1, 2, 3, 4, 5, 10, 15, 14, 13, 12, 11, 6, 7, 8, 9]

spiralOrder([
  [1, 2, 3, 4, 5, 6],
  [7, 8, 9, 10, 11, 12],
  [13, 14, 15, 16, 17, 18],
]);
//  ➞ [1, 2, 3, 4, 5, 6, 12, 18, 17, 16, 15, 14, 13, 7, 8, 9, 10, 11]

spiralOrder([
  [13, 14, 15, 16, 17, 18],
  [1, 2, 3, 4, 5, 6],
  [7, 8, 9, 10, 11, 12],
]);
// ➞ [13, 14, 15, 16, 17, 18, 6, 12, 11, 10, 9, 8, 7, 1, 2, 3, 4, 5]
spiralOrder([
  [13, 14, 15, 16, 17, 18, 19, 20, 21, 22],
  [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
  [7, 8, 9, 10, 11, 12, 23, 24, 25, 26],
]);
// ➞ [13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 10, 26, 25, 24, 23, 12, 11, 10, 9, 8, 7, 1, 2, 3, 4, 5, 6, 7, 8, 9]
