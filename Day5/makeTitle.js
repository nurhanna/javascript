// =================================================================================================================
// Create a function that takes a string as an argument and converts the first character of each word to uppercase. Return the newly formatted string.

// Notes
// You can expect a valid string for each test case.
// Some words may contain more than one uppercase letter (see example #4)

const makeTitle = (title) => {
  let myArray = title.split(" ");
  for (let i = 0; i < myArray.length; i++) {
    myArray[i] = myArray[i].charAt(0).toUpperCase() + myArray[i].slice(1);
  }
  console.log(myArray);
};

makeTitle("This is a title");
makeTitle("capitalize every word");
makeTitle("I Like Pizza");
makeTitle("PIZZA PIZZA PIZZA");
makeTitle("I am a title");
makeTitle("I AM A TITLE");
makeTitle("i aM a tITLE");
makeTitle("the first letter of every word is capitalized");
makeTitle("I Like Pizza");

makeTitle("1f you c4n r34d 7h15, you r34lly n33d 2 g37 l41d");
