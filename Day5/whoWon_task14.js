// 14. Create a function that takes a Tic-tac-toe board and returns "X" if the X's are placed in a way that there are three X's in a row or returns "O" if there are three O's in a row.
// Notes:
// All places on the board will have either "X" or "O". If both "X" and "O" win, return "Tie".

const whoWon = (arr) => {
  let score_o = 0;
  let score_x = 0;

  //   untuk yang vertikal & horizontal

  for (let i = 0; i < arr.length; i++) {
    if (arr[i][0] == arr[i][1] && arr[i][0] == arr[i][2]) {
      if (arr[i][1] == "O") {
        score_o += 1;
      } else {
        score_x += 1;
      }
    }
    if (arr[0][i] == arr[1][i] && arr[0][i] == arr[2][i]) {
      if (arr[0][i] == "O") {
        score_o += 1;
      } else {
        score_x += 1;
      }
    }
  }

  //   untuk yang silang

  if (arr[0][0] == arr[1][1] && arr[0][0] == arr[2][2]) {
    if (arr[0][0] == "O") {
      score_o += 1;
    } else {
      score_x += 1;
    }
    i = arr.length;
  } else if (arr[2][2] == arr[1][1] && arr[2][2] == arr[0][0]) {
    if (arr[2][2] == "O") {
      score_o += 1;
    } else {
      score_x += 1;
    }
    i = arr.length;
  } else if (arr[2][0] == arr[1][1] && arr[2][0] == arr[0][2]) {
    if (arr[2][0] == "O") {
      score_o += 1;
    } else {
      score_x += 1;
    }
    i = arr.length;
  }
  if (score_o > score_x) {
    console.log("O");
  } else if (score_o < score_x) {
    console.log("X");
  } else {
    console.log("Tie");
  }
};

// success all test case

whoWon([
  ["O", "X", "O"],
  ["X", "X", "O"],
  ["O", "X", "X"],
]); //➞ "X"
whoWon([
  ["O", "O", "X"],
  ["X", "O", "X"],
  ["O", "X", "O"],
]); //➞ "O"
whoWon([
  ["O", "O", "X"],
  ["X", "X", "O"],
  ["O", "X", "O"],
]); // ➞ "Tie"
whoWon([
  ["X", "O", "X"],
  ["X", "O", "O"],
  ["X", "X", "O"],
]); //➞ "X"
whoWon([
  ["O", "X", "O"],
  ["X", "X", "O"],
  ["O", "X", "X"],
]); // ➞ "X"
whoWon([
  ["X", "X", "O"],
  ["O", "X", "O"],
  ["X", "O", "O"],
]); //➞ "O"

whoWon([
  ["X", "X", "X"],
  ["O", "X", "O"],
  ["X", "O", "O"],
]); //➞ "X"
whoWon([
  ["X", "O", "X"],
  ["O", "O", "O"],
  ["X", "X", "O"],
]); //➞ "O"
whoWon([
  ["O", "O", "X"],
  ["X", "O", "X"],
  ["O", "X", "O"],
]); // ➞ "O"
whoWon([
  ["O", "O", "X"],
  ["O", "X", "X"],
  ["X", "X", "O"],
]); //➞ "X"
whoWon([
  ["O", "O", "X"],
  ["X", "X", "X"],
  ["O", "O", "O"],
]); // ➞ "Tie"
whoWon([
  ["O", "O", "X"],
  ["X", "X", "O"],
  ["O", "X", "O"],
]); //➞ "Tie"
