// 6. An array is special if every even index contains an even number and every odd index contains an odd number. Create a function that returns true if an array is special, and false otherwise.

const isSpecialArray = (arr) => {
  let nilai = true;
  for (let i = 0; i < arr.length; i++) {
    if (i % 2 == 0) {
      if (arr[i] % 2 != 0) {
        nilai = false;
      }
    }
    if (i % 2 != 0) {
      if (arr[i] % 2 == 0) {
        nilai = false;
      }
    }
  }
  console.log(nilai);
};

isSpecialArray([2, 7, 4, 9, 6, 1, 6, 3]);
isSpecialArray([2, 7, 9, 1, 6, 1, 6, 3]);
isSpecialArray([2, 7, 8, 8, 6, 1, 6, 3]);
isSpecialArray([2, 7, 4, 9, 6, 1, 6, 3]);
isSpecialArray([2, 7, 9, 1, 6, 1, 6, 3]);
isSpecialArray([2, 7, 8, 8, 6, 1, 6, 3]);
isSpecialArray([1, 1, 1, 2]);
isSpecialArray([2, 2, 2, 2]);
isSpecialArray([2, 1, 2, 1]);
isSpecialArray([4, 5, 6, 7]);
isSpecialArray([4, 5, 6, 7, 0, 5]);
