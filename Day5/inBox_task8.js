// 8. Create a function that returns true if an asterisk * is inside a box.
// Notes
// The asterisk may be in the array, however, it must be inside the box, if it exists.

const inBox = (arr) => {
  let nilai = true;
  let jml_bintang = 0;
  for (let i = 0; i < arr.length; i++) {
    //   cek di array pertama tidak boleh ada *
    if (arr[0].includes("*")) {
      nilai = false;
      i == arr.length; //jika sudah false hentikan looping
    }
    //   cek di array terakhir tidak boleh ada *
    if (arr[arr.length - 1].includes("*")) {
      nilai = false;
      i == arr.length;
    }

    // cek ada bintang
    if (arr[i].includes("*")) {
      jml_bintang += 1;
    }
  }
  //   * tidak boleh tidak ada sama sekali
  if (jml_bintang < 1) {
    nilai = false;
  }
  console.log(nilai);
};

// all test case success

inBox(["###", "#*#", "###"]); //➞ true
inBox(["####", "#* #", "#  #", "####"]); //➞ true
inBox(["*####", "# #", "#  #*", "####"]); //➞ false
inBox(["#####", "#   #", "#   #", "#   #", "#####"]); //➞ false
inBox(["###", "# #", "###"]); //➞ false
inBox(["####", "#  #", "#  #", "####"]); //➞ false
inBox(["#####", "#   #", "#   #", "#   #", "#####"]); //➞ false
inBox(["###", "#*#", "###"]); //➞ true
inBox(["####", "# *#", "#  #", "####"]); //➞ true
inBox(["#####", "#  *#", "#   #", "#   #", "#####"]); // ➞ true
inBox(["#####", "#   #", "# * #", "#   #", "#####"]); // ➞ true
inBox(["#####", "#   #", "# * #", "#   #", "#####"]); //➞ true
inBox(["#####", "#*  #", "#   #", "#   #", "#####"]); //➞ true
