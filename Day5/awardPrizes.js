// 9.You are given a dictionary of names and the amount of points they have. Return a dictionary with the same names, but instead of points, return what prize they get.
// "Gold", "Silver", or "Bronze" to the 1st, 2nd and 3rd place respectively. For all the other names, return "Participation" for the prize.

// Notes
// There will always be at least three participants to recieve awards.
// No number of points will be the same amongst participants.

const awardPrizes = (obj) => {
  // console.log(Math.max(Object.values(obj)));
  let key = Object.keys(obj);
  let value = Object.values(obj);
  let last_obj = [];
  for (let i = 0; i < key.length; i++) {
    let newObj = {};
    newObj["name"] = key[i];
    newObj["point"] = value[i];
    newObj["index_awal"] = i;
    last_obj.push(newObj);
  }

  let obj_sort = last_obj.sort((a, b) => {
    if (a.point < b.point) {
      return 1;
    } else if (a.point > b.point) {
      return -1;
    } else {
      return 0;
    }
  });

  for (let i = 0; i < key.length; i++) {
    if (i == 0) {
      obj_sort[i]["prize"] = "Gold";
    } else if (i == 1) {
      obj_sort[i]["prize"] = "Silver";
    } else if (i == 2) {
      obj_sort[i]["prize"] = "Bronze";
    } else {
      obj_sort[i]["prize"] = "Participation";
    }
  }

  //   ballikan lagi sesuai urutan input awal
  obj_sort.sort((a, b) => {
    if (a.index_awal < b.index_awal) {
      return -1;
    } else if (a.index_awal > b.index_awal) {
      return 1;
    } else {
      return 0;
    }
  });

  let final = {};
  for (let i = 0; i < key.length; i++) {
    final[obj_sort[i].name] = obj_sort[i].prize;
  }

  console.log(final);
};

awardPrizes({
  Joshua: 45,
  Alex: 39,
  Eric: 43,
});
awardPrizes({
  "Person A": 1,
  "Person B": 2,
  "Person C": 3,
  "Person D": 4,
  "Person E": 102,
});

awardPrizes({
  Mario: 99,
  Luigi: 100,
  Yoshi: 299,
  Toad: 2,
});

awardPrizes({
  Joshua: 45,
  Alex: 39,
  Eric: 43,
});

awardPrizes({
  "Person A": 1,
  "Person B": 2,
  "Person C": 3,
  "Person D": 4,
  "Person E": 102,
});

awardPrizes({
  Mario: 99,
  Luigi: 100,
  Yoshi: 299,
  Toad: 2,
});
