// Check if property exists in object
// Write a function that takes an object (a) and a string (b) as argument. Return true if the object has a property with key 'b'. Return false otherwise. Tipp: test case 3 is a bit tricky because the value of property 'z' is undefined. But the property itself exists.

// function myFunction(a, b) {
//   if (b in a) {
//     console.log(true);
//   } else if (!a[b]) {
//     console.log(false);
//   } else {
//     console.log(true);
//   }
// }

// myFunction({ a: 1, b: 2, c: 3 }, "b");
// myFunction({ x: "a", y: "b", z: "c" }, "a");
// myFunction({ x: "a", y: "b", z: undefined }, "z");

// // jawaban mereka
// function myFunction(a, b) {
//   return b in a;
// }

// ==================================================================================================================================

// Check if property exists in object and is truthy
// Write a function that takes an object (a) and a string (b) as argument. Return true if the object has a property with key 'b', but only if it has a truthy value. In other words, it should not be null or undefined or false. Return false otherwise.

// function myFunction(a, b) {
//   if (a[b]) {
//     console.log(true);
//   } else {
//     console.log(false);
//   }
// }

// myFunction({ a: 1, b: 2, c: 3 }, "b");
// myFunction({ x: "a", y: null, z: "c" }, "y");
// myFunction({ x: "a", b: "b", z: undefined }, "z");

// // jawaban mereka
// function myFunction(a, b) {
//   return Boolean(a[b]);
// }

// ========================================================================================================================================

// Creating Javascript objects one
// Write a function that takes a string as argument. Create an object that has a property with key 'key' and a value equal to the string. Return the object.

// function myFunction(a) {
//   console.log(new Object({ key: a }));
// }

// myFunction("a");
// myFunction("z");
// myFunction("b");

// // jawaban mereka
// function myFunction(a) {
//   return { key: a };
// }

// ======================================================================================================================================

// Creating Javascript objects two
// Write a function that takes two strings (a and b) as arguments. Create an object that has a property with key 'a' and a value of 'b'. Return the object.

// function myFunction(a, b) {
//   obj = {};
//   obj[a] = b;
//   console.log(obj);
// }

// myFunction("a", "b");
// myFunction("z", "x");
// myFunction("b", "w");

// // jawaban mereka

// function myFunction(a, b) {
//   return { [a]: b };
// }

// ===========================================================================================================================================

// Creating Javascript objects three
// Write a function that takes two arrays (a and b) as arguments. Create an object that has properties with keys 'a' and corresponding values 'b'. Return the object.

// function myFunction(a, b) {
//   obj = {};
//   for (let i = 0; i < a.length; i++) {
//     obj[a[i]] = b[i];
//   }
//   console.log(obj);
// }

// myFunction(["a", "b", "c"], [1, 2, 3]);
// myFunction(["w", "x", "y", "z"], [10, 9, 5, 2]);
// myFunction([1, "b"], ["a", 2]);

// // jawaban mereka
// function myFunction(a, b) {
//   return a.reduce((acc, cur, i) => ({ ...acc, [cur]: b[i] }), {});
// }

// =========================================================================================================================================
//Extract keys from Javascript object
// Write a function that takes an object (a) as argument. Return an array with all object keys.

// function myFunction(a) {
//   console.log(Object.keys(a));
// }

// myFunction({ a: 1, b: 2, c: 3 });
// myFunction({ j: 9, i: 2, x: 3, z: 4 });
// myFunction({ w: 15, x: 22, y: 13 });

// jawaban benar

// ========================================================================================================================================

// Return nested object property
// Write a function that takes an object as argument. In some cases the object contains other objects with some deeply nested properties. Return the property 'b' of object 'a' inside the original object if it exists. If not, return undefined

// function myFunction(obj) {
//   const value = Object.entries(obj).map((isi) => isi[1].b);
//   console.log(value[0]);
// }

// myFunction({ a: 1 });
// myFunction({ a: { b: { c: 3 } } });
// myFunction({ b: { a: 1 } });
// myFunction({ a: { b: 2 } });

// // jawaban mereka
// function myFunction(obj) {
//   return obj?.a?.b;
// }

// ========================================================================================================================================
// Sum object values
// Write a function that takes an object (a) as argument. Return the sum of all object values.

// function myFunction(a) {
//   let sum = 0;
//   const value = Object.values(a);
//   for (let i = 0; i < value.length; i++) {
//     sum += value[i];
//   }
//   console.log(sum);
// }

// myFunction({ a: 1, b: 2, c: 3 });
// myFunction({ j: 9, i: 2, x: 3, z: 4 });
// myFunction({ w: 15, x: 22, y: 13 });

// // jawaban mereka

// function myFunction(a) {
//   return Object.values(a).reduce((sum, cur) => sum + cur, 0);
// }

// =======================================================================================================================================

// Remove a property from an object
// Write a function that takes an object as argument. It should return an object with all original object properties. except for the property with key 'b'

// function myFunction(obj) {
//   delete obj.b;
//   console.log(obj);
// }

// myFunction({ a: 1, b: 7, c: 3 });
// myFunction({ b: 0, a: 7, d: 8 });
// myFunction({ e: 6, f: 4, b: 5, a: 3 });

// // jawaban mereka

// function myFunction(obj) {
//   const { b, ...rest } = obj;
//   return rest;
// }

// =======================================================================================================================================
// Merge two objects with matching keys
// Write a function that takes two objects as arguments. Unfortunately, the property 'b' in the second object has the wrong key. It should be named 'd' instead. Merge both objects and correct the wrong property name. Return the resulting object. It should have the properties 'a', 'b', 'c', 'd', and 'e'

// function myFunction(x, y) {
//   // tambah elemen baru d pakai nilai b
//   y.d = y.b;
//   //   delete b di obj y
//   delete y.b;
//   //   merge
//   let gabungan = {
//     ...x,
//     ...y,
//   };

//   console.log(gabungan);
// }

// myFunction({ a: 1, b: 2 }, { c: 3, b: 4, e: 5 });
// myFunction({ a: 5, b: 4 }, { c: 3, b: 1, e: 2 });

// // jawaban mereka

// function myFunction(x, y) {
//   const { b, ...rest } = y;
//   return { ...x, ...rest, d: b };
// }

// ==========================================================================================================================================

// Multiply all object values by x
// Write a function that takes an object (a) and a number (b) as arguments. Multiply all values of 'a' by 'b'. Return the resulting object.

function myFunction(a, b) {
  let obj_baru = {};
  for (name in a) {
    obj_baru[name] = a[name] * b;
  }
  console.log(obj_baru);
}

myFunction({ a: 1, b: 2, c: 3 }, 3);
myFunction({ j: 9, i: 2, x: 3, z: 4 }, 10);
myFunction({ w: 15, x: 22, y: 13 }, 6);

// jawaban mereka
function myFunction(a, b) {
  return Object.entries(a).reduce((acc, [key, val]) => {
    return { ...acc, [key]: val * b };
  }, {});
}
