// Remove a specific array element // BELUM BERHASIL
// Write a function that takes an array (a) and a value (b) as argument. The function should remove all elements equal to 'b' from the array. Return the filtered array.

// function myFunction(a, b) {
//   for (let i = 0; i < a.length; i++) {
//     if (a[i] === b) {
//       // At position i, remove a items:
//       a.splice(i, 1);
//     }
//   }
//   console.log(a);
// }

// myFunction([1, 2, 3], 2);
// myFunction([1, 2, "2"], "2");
// myFunction([false, "2", 1], false);
// myFunction([1, 2, "2", 1], 1);

// jawaban mereka
// function myFunction( a, b ) {
//     return a.filter(cur => cur !== b)
//   }

// ======================================================================================================================

// Sort an array of strings alphabetically
// Write a function that takes an array of strings as argument. Sort the array elements alphabetically. Return the result.

// function myFunction(arr) {
//   arr.sort();
//   console.log(arr);
// }
// myFunction(["b", "c", "d", "a"]);
// myFunction(["z", "c", "d", "a", "y", "a", "w"]);

// =========================================================================================================================

// Sort an array of numbers in descending order
// Write a function that takes an array of numbers as argument. It should return an array with the numbers sorted in descending order.

// function myFunction(arr) {
//   arr.sort();
//   console.log(arr.reverse());
// }

// myFunction([1, 3, 2]);
// myFunction([4, 2, 3, 1]);

// // jawaban
// function myFunction( arr ) {
//     return arr.sort((a, b) => b - a)
//   }

// =====================================================================================================================================

// Return the longest string from an array of strings
// Write a function that takes an array of strings as argument. Return the longest string.

// function myFunction(arr) {
//   let p = 0;
//   let word;
//   for (let i = 0; i < arr.length; i++) {
//     if (p < arr[i].length) {
//       p = arr[i].length;
//       word = arr[i];
//     }
//   }
//   console.log(word);
// }

// myFunction(["help", "me"]);
// myFunction(["I", "need", "candy"]);

// // jawaban mereka

// function myFunction(arr) {
//   return arr.reduce((a, b) => (a.length <= b.length ? b : a));
// }

// =======================================================================================================================================
// Check if all array elements are equal
// Write a function that takes an array as argument. It should return true if all elements in the array are equal. It should return false otherwise.

// function myFunction(arr) {
//   first = true;
//   for (let i = 0; i < arr.length; i++) {
//     for (j = i; j < arr.length - i; j++) {
//       if (arr[i] !== arr[j]) {
//         first = false;
//         break;
//       }
//     }
//   }
//   console.log(first);
// }

// myFunction([true, true, true, true]);
// myFunction(["test", "test", "test"]);
// myFunction([1, 1, 1, 2]);
// myFunction(["10", 10, 10, 10]);

// // jawaban mereka
// function myFunction( arr ) {
//     return new Set(arr).size === 1
//   }

// ==================================================================================================================================

// Merge an arbitrary number of arrays
// Write a function that takes arguments an arbitrary number of arrays. It should return an array containing the values of all arrays.

// function myFunction(...arrays) {
//   //   console.log(arrays);
//   let arrays2 = arrays;
//   let array3 = [];
//   arrays2.forEach((a) => {
//     a.forEach((b) => {
//       array3.push(b);
//     });
//   });
//   console.log(array3);
// }

// myFunction([1, 2, 3], [4, 5, 6]);
// myFunction(["a", "b", "c"], [4, 5, 6]);
// myFunction([true, true], [1, 2], ["a", "b"]);

// // jawaban
// function myFunction( ...arrays ) {
//     return arrays.flat()
//     }

// ===============================================================================================================================

// Sort array by object property
// Write a function that takes an array of objects as argument. Sort the array by property b in ascending order. Return the sorted array

// function myFunction(arr) {
//   //   console.log(arr);
//   if (arr[0].b > arr[1].b) {
//     arr.reverse();
//   }
//   console.log(arr);
// }

// myFunction([
//   { a: 1, b: 2 },
//   { a: 5, b: 4 },
// ]);
// myFunction([
//   { a: 2, b: 10 },
//   { a: 5, b: 4 },
// ]);
// myFunction([
//   { a: 1, b: 7 },
//   { a: 2, b: 1 },
// ]);

// // jawaban mereka

// function myFunction(arr) {
//   const sort = (x, y) => x.b - y.b;
//   return arr.sort(sort);
// }

// =============================================================================================================================================

// Merge two arrays with duplicate values
// Write a function that takes two arrays as arguments. Merge both arrays and remove duplicate values. Sort the merge result in ascending order. Return the resulting array

// function myFunction(a, b) {
//   let gabung = a.concat(b);
//   console.log("gabung");
//   console.log(gabung);
//   console.log("hilang duplicat");
//   gabung = new Set(gabung);
//   console.log(gabung);
//   //   console.log("yang di sort");
//   //   console.log(gabung.sort());
//   console.log("convert set to array");
//   gabung = Array.from(gabung);
//   console.log(gabung);
//   console.log("sort");
//   gabung.sort(
//     (compare = (a, b) => {
//       return a - b;
//     })
//   );
//   console.log(gabung);
// }

// tanpa console.log

//  let gabung = a.concat(b);
//   gabung = new Set(gabung);
//   gabung = Array.from(gabung);
//   gabung.sort(
//     (compare = (a, b) => {
//       return a - b;
//     })
//   );

// myFunction([1, 2, 3], [3, 4, 5]);
// myFunction([-10, 22, 333, 42], [-11, 5, 22, 41, 42]);

// jawaban mereka

// function myFunction(a, b) {
//     return [...new Set([...a, ...b])].sort((x, y) => x - y);
//   }

// =========================================================================================================================================
